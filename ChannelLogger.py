import logging
#import time
import logging.handlers
import os
import time
#from logging.handlers import RotatingFileHandler
class ChannelLogger:
    def __init__(self,path="logs",default=logging.INFO):
        self.channels = []
        self.idx = {}
        self.loggers ={}
        self.path = path
        self.default = default
                
    def create_loggers(self,channel_list):
        for channel in channel_list:
            self._create_logger(channel.name,channel.id)                

    def _create_logger(self,name,ident,level=logging.INFO):
        self.channels.append(name)
        logger = logging.getLogger("Rotating Log for " + name)

        #simple formatting will do
        formatter = logging.Formatter('%(asctime)s-%(message)s')

        #declare path
        path = self.path + "/" + name
        log_file_name = path + "/" + name + ".log"

        #make one if not 2
        if not os.path.exists(path):
            try:
                os.makedirs(path)
            except Exception:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                print(exc_type, exc_value, exc_traceback)

        #give out handlers
        handler = logging.handlers.TimedRotatingFileHandler(log_file_name, when="midnight" , backupCount=10)
        handler.setFormatter(formatter)
        logger.addHandler(handler)
        logger.setLevel(level)

        #why do we reset this?
        handler = None
        
        #assume all unique channel ids map to a unique name  
        self.idx[name]=ident

        #add to logger
        self.loggers[ident]=logger

    def logByName(self,name,content):
        try:
            idx = self.idx[name]
            logger = self.loggers[idx]
        except:
            return
        logger.info(content)

        
    def logByIdent(self,ident,content):
        try:
            logger = self.loggers[ident]
            logger.info(content)
        except:
            return
        
if __name__ == "__main__":
    log_file = "test.log"
    logger = ChannelLogger()
    logger._create_logger(name="abcd",ident="1234")
    for i in range(10000):
        time.sleep(0.1)
        logger.logByIdent(content="test",ident="1234")
