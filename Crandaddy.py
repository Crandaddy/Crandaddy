#from html.parser import HTMLParser
import urllib.request as urllib2
import datetime
import re
import discord
import asyncio
#import logging
import random
from stbedes import *
import calendar
import praw
import json
import atexit
import aiohttp
import os, errno
import pathlib
import requests
import logging
from  ChannelLogger import ChannelLogger


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('discord')
#logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(filename='discord.log', encoding='utf-8', mode='w')
handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
logger.addHandler(handler)
#import logging
#from RedditClient import RedditClient
"""
Crandaddy is subject to breakage 
"""

        
"""
Check out config.json.example for how to setup
"""


"""
Just to setup Error
"""
class Crandaddy(discord.Client):
    def __init__(self, *args, loop=None,config = None,memes=None, **kwargs):
        super().__init__(*args, loop=loop, **kwargs)
        #self.loop=asyncio.get_event_loop()
  #      self.bg_task = self.loop.create_task(self.bg())
        print("task created")
        print(self.loop)
        self.current_page = None
        self.total_pages = None
        self.config = config
        self.memes = memes
        self.channels = None
        self.ChannelLogger = ChannelLogger()
    def process_flags(self,string):
        book = None
        yesterday = False
        
        match1 = re.compile('1979')
        result = match1.search(string)
        if (not (result == None)):
            book = "1979"
        
        match2 = re.compile('yesterday', re.IGNORECASE)
        result = match2.search(string)
        if (not (result == None)):
            yesterday = True
        return book,yesterday

    def log_message(self,message):
        path = config["logging"]["path"]
    
        channel = message.channel
        ident = channel.id
        author = message.author
        content = message.content
        display_name = author.display_name
        user_id = author.id
        name = author.name
        string = "({:s},{:s},{:s}): {:s}".format(name,display_name,user_id,message.content)
        print(string)
        self.ChannelLogger.logByIdent(content=string,ident=ident)
        
    

    
    def get_date(self,yesterday=False):
        utc_datetime  = datetime.datetime.utcnow()        
        year = utc_datetime.year
        month = utc_datetime.strftime("%B")
        month2= utc_datetime.month
        day = utc_datetime.day
        week = calendar.day_name[utc_datetime.weekday()]

        if(yesterday):
            day = day - 1
            week = calendar.day_name[utc_datetime.weekday()-1]
        
        return year,month,month2,day,week

        
    async def on_ready(self):
        self.bg_task = self.loop.create_task(self.bg())

        server = list(self.servers)
        channels = list(server[0].channels)        
        self.ChannelLogger.create_loggers(channels)
        print(self.ChannelLogger.loggers)
     #   pass
     #   channel = self.get_channel(self.config["welcome"]["channel"])
     #   #await self.send_message(channel,"I will arise and go to my father, and will say unto him, Father, I have sinned against heaven, and before thee")

    async def reload_memes(self,message):
        #get server 
        server = message.server
        roles = server.roles
        #print(roles)
        
        try:
            admin_channel =self.get_channel(self.config["admin"]["channel"])
        except KeyError:
            admin_channel =self.get_channel("admin")
        print(message.channel)
        print(admin_channel)
        if not (message.channel == admin_channel):
            print("failed")
            pass        
        else:
            with open('config/memes.json') as json_data:
                memes = json.load(json_data)
                self.memes = memes
                await self.send_message(admin_channel, "Memes reloaded")

    async def add_memes(self,message):
        #get server 
        server = message.server
        roles = server.roles
        #print(roles)
        
        try:
            admin_channel =self.get_channel(self.config["admin"]["channel"])
        except KeyError:
            admin_channel =self.get_channel("admin")

            
        print(message.channel)
        print(admin_channel)
        if not (message.channel == admin_channel):
            print("failed")
            pass        
        else:
            #need more arugments
            args = message.content.split(' ')
            if(len(args) < 3):
                await self.send_message(admin_channel, "Not enough arguments")
                return
            #otherwise 
            with open('config/memes.json',mode='r') as json_data:
                memes = json.load(json_data)

            with open('config/memes.json',mode='w') as json_data:
                memes[args[1]]={}
                memes[args[1]]["url"]=args[2]
                self.memes = memes
                json.dump(memes, json_data)
                await self.send_message(admin_channel, "Meme added")

                
    async def post_memes(self,message):
        if message.content.startswith('!'):
            string = message.content.split(' ')
            command = string[0][1:]
        
            try:
                url = self.memes[command]["url"]
                e = discord.Embed()
                e.set_image(url=url)
                await self.send_message(message.channel, embed=e)
                return 
            except KeyError : 
                print("URL doesn't exist")
            
            try:
                path = self.config["files"]["imagedir"] + "/" + self.memes[command]["file"]
                await self.send_file(message.channel,path,content="",filename=self.memes[command]["file"] )
                return 
            except KeyError :                
                print("File doesn't exist")
                #await self.send_message(message.channel, "The meme doesn't exist")
        else:
            pass    
    async def on_message(self,message):
        await self.wait_until_ready()
        self.log_message(message)
        #print(message.content)
        #Morning Prayer

        if message.content.startswith('!add'):
            await self.add_memes(message)
            return 
        if message.content.startswith('!reload'):
            await self.reload_memes(message)
            return

        if message.content.startswith('!prayforus'):
            args = message.content.split(' ',1)
            string = "```\nBlessed {0},pray for us.\n```".format(args[1])
            await self.send_message(message.channel, string)
            return 
    
        
        
        if message.content.startswith('!humbleaccess'):
            string = self.config['humbleaccess']['1549']
            await self.send_message(message.channel, string)
            return 
            
        if message.content.startswith('!help'):
            string = " Crandaddy commands : ``` "
            string = string +  "!LP : Lord's Prayer \n !creed : Nicene Creed \n !MP : Morning Prayer \n !EP : Evening Prayer \n !memes : A List of Memes ```"
            await self.send_message(message.channel, string)
            
        if message.content.startswith('!MP'):
            book, yesterday  = self.process_flags(message.content)
            year,month,month2,day,week = self.get_date(yesterday)
        
            if(str(book) == "1979"): 
                fmt = 'd={0}&m={1}&y={2}'
                date = fmt.format(day,month2,year)
                URL="http://prayer.forwardmovement.org/daily_prayer.php?" + date + "&office=MP"
            else:
                date = week + "-" + str(day) + "-" + str(month) + "-" + str(year) 
                URL= "https://www.churchofengland.org/prayer-and-worship/join-us-in-daily-prayer/morning-prayer-traditional-" + date
            
            await self.send_message(message.channel, URL)

            #post the Nicene Creed without Filioque (replace contents in creed.txt for any theologically orthodox creed)
        elif (message.content.startswith('!CREED') or message.content.startswith('!creed')):
            fi = open("creed.txt","r")
            string = fi.read()
            await self.send_message(message.channel, string)
            fi.close()

            #post the Nicene Creed without Filioque (replace contents in creed.txt for any theologically orthodox creed)
        elif (message.content.startswith('!LP') or message.content.startswith('!lp')):
            fi = open("lordsprayer.txt","r")
            string = fi.read()
            await self.selfend_message(message.channel, string)
            fi.close()
        
            #Evening Prayer
        elif message.content.startswith('!memes'):
            keys = self.memes.keys()
            print(self.memes)
            string = "Avaliable memes are: ``` \n!" + "\n!".join(str(i) for i in keys) + "```"
            await self.send_message(message.channel, string)
        elif message.content.startswith('!EP'):
            print(message.content)
            book, yesterday  = self.process_flags(message.content)    
            year,month,month2,day,week = self.get_date(yesterday)

            if(str(book) == "1979"):
                fmt = 'd={0}&m={1}&y={2}'
                date = fmt.format(day,month2,year)
                URL="http://prayer.forwardmovement.org/daily_prayer.php?" + date + "&office=EP"
            else:
                date = week + "-" + str(day) + "-" + str(month) + "-" + str(year)
                URL= "https://www.churchofengland.org/prayer-and-worship/join-us-in-daily-prayer/evening-prayer-traditional-" + date
            await self.send_message(message.channel, URL)
        else:
            await self.post_memes(message)
            
    async def on_member_join(self,member):
        fi = open("welcome.txt","r")
        string = fi.read()
        fi.close()
        server = member.server
        channel = self.get_channel(self.config["welcome"]["channel"])
        
        fmt = 'Welcome {0.mention} to {1.name}!\nYou have seen the Crandaddy!\n'
        welcome = fmt.format(member, server)
        message = welcome + string
        await self.send_message(channel, message)

    async def bg(self):
        await self.wait_until_ready()
        print("loop status:" )
        print(self.loop)
        print("====")
        print(self.loop)
        print(self.is_closed)
        closed = self.is_closed
        server = list(self.servers)
        channels = list(server[0].channels)
        for i in channels:
            print(i.name)
        #print(server[0].channels)
        while not closed:
            print("sleeping")
            await asyncio.sleep(3600)
            
        
                
config = None
memes = None
with open('config/config.json') as json_data:
    config = json.load(json_data)
    #print(config)
    
with open('config/memes.json') as json_data:
    memes = json.load(json_data)
    
#loop = asyncio.get_event_loop()    
Crandaddy = Crandaddy(config=config,memes=memes)
try:
    Crandaddy.run(config["discord"]["api_key"])
except KeyboardInterrupt:
    exit(1)
except TypeError:
    exit(1)
